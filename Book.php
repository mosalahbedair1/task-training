<?php

  require_once('products.php');

  class Book extends Product
  {
      protected $weight;
      
      protected function setWeight($weight)
      {
          $this->weight=$weight;
      }

      public function addProduct($book)
      {
          $conn = db();
          $this->setSKU($book['SKU']);
          $this->setName($book['name']);
          $this->setPrice($book['price']);
          $this->setType($book['type']);
          $this->setWeight($book['weight']);
          $sql = "INSERT INTO product (SKU,pname,price,ptype,psize,pheight,pwidth,plength,pweight) VALUES ('$this->SKU','$this->name','$this->price','$this->type',0,0,0,0,'$this->weight')";
          if (mysqli_query($conn, $sql)) {
              header('Location:index.php');
          }
          mysqli_close($conn);
      }
  }
