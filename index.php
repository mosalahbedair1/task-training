<?php
  include('productHandler.php');
  $pro = new Handler();
  $products = $pro->getProducts();
  if (isset($_POST['massdelete'])) {
      $checkBoxs = count($_POST['records']);
      $pro->deleteRecords($_POST, $checkBoxs);
  }
?>



<!DOCTYPE html>
<html lang="en">
<?php include('templates/header.php') ?>

<div class="container">
  <form
    action="<?php echo $_SERVER['PHP_SELF']?>"
    method="POST">


    <!-- Start Header -->
    <nav>
      <h1>Product List</h1>
      <div class='btns-container'>
        <button type='button' class='btn btn-primary' onclick='window.location.href = "add.php";'>ADD</button>
        <button type="submit" class="btn btn-danger" id="delete-product-btn" name="massdelete" value="Delete">
          MASS DELETE
        </button>
      </div>
    </nav>
    <hr>
    <!-- End Header -->


    <!-- Start Cards Container -->
    <div class='products-cards'>
      <?php if (!empty($products)) {?>
      <?php foreach ($products as $product) {?>
      <!-- Start Product Card -->
      <div class="product-card">
        <input class="form-check-input delete-checkbox" type="checkbox" name="records[]"
          value="<?php echo htmlspecialchars($product['SKU']); ?>">

        <!-- Start DVD Card -->
        <?php if (htmlspecialchars($product['ptype'])=="DVD") { ?>
        <h5 class="sku"><?php echo htmlspecialchars($product['SKU']) ?>
        </h5>
        <h6 class="type text-muted"><?php echo htmlspecialchars($product['pname']) ?>
        </h6>
        <p class="price">
          <?php
          /*
          This if statement is to add '.00' to the price if it isn't a decimal value already, I used strpos() method to handle this,
          because the input type text gives a string, not a number can be checked if float or not by is_float() method
          */
          if (strpos($product['price'], '.') === false) {
              echo $product['price'] . '.00' . '$';
          } else {
              echo $product['price'] . '$';
          }
          ?>
        </p>
        <p class="attribute"><?php echo 'Size: '. $product['psize'] .'MB' ?>
        </p>
        <?php } ?>
        <!-- End DVD Card -->


        <!-- Start Furniture Card -->
        <?php if (htmlspecialchars($product['ptype'])=="Furniture") { ?>
        <h5 class="sku"><?php echo htmlspecialchars($product['SKU']) ?>
        </h5>
        <h6 class="type text-muted"><?php echo htmlspecialchars($product['pname']) ?>
        </h6>
        <p class="price">
          <?php
          /*
          This if statement is to add '.00' to the price if it isn't a decimal value already, I used strpos() method to handle this,
          because the input type text gives a string, not a number can be checked if float or not by is_float() method
          */
          if (strpos($product['price'], '.') === false) {
              echo $product['price'] . '.00' . '$';
          } else {
              echo $product['price'] . '$';
          }
          ?>
        </p>
        <p class="attribute"><?php echo 'Dimensions: '. $product['pheight'] .'x'. $product['pwidth'] .'x'. $product['plength'] ?>
        </p>
        <?php } ?>
        <!-- End Furniture Card -->


        <!-- Start Book Card -->
        <?php if (htmlspecialchars($product['ptype'])=="Book") { ?>
        <h5 class="sku"><?php echo htmlspecialchars($product['SKU']) ?>
        </h5>
        <h6 class="type text-muted"><?php echo htmlspecialchars($product['pname']) ?>
        </h6>
        <p class="price">
          <?php
          /*
          This if statement is to add '.00' to the price if it isn't a decimal value already, I used strpos() method to handle this,
          because the input type text gives a string, not a number can be checked if float or not by is_float() method
          */
          if (strpos($product['price'], '.') === false) {
              echo $product['price'] . '.00' . '$';
          } else {
              echo $product['price'] . '$';
          }
          ?>
        </p>
        <p class="attribute"><?php echo 'Weight: '. $product['pweight'] .'KG' ?>
        </p>
        <?php } ?>
        <!-- End Book Card -->

      </div>
      <!-- End Product Card -->
      <?php } ?>
      <?php } ?>
    </div>
    <!-- End Cards Container -->
  </form>
</div>

<?php include('templates/footer.php') ?>

</html>